package v1

import (
	"client/internal/domain/auth/queries"
	"client/internal/domain/auth/service"
	"client/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type handler struct {
	log *logger.Logger
	svc *service.ProductService
	v   *validator.Validate
}

func NewHandler(log *logger.Logger, svc *service.ProductService, v *validator.Validate) *handler {
	return &handler{log, svc, v}
}

func (h handler) Registration(c *gin.Context) {
	input := &queries.RegistrationQuery{}
	if err := c.BindJSON(input); err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	if err := h.v.Struct(input); err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": err.Error()})
		return
	}
	tokens, err := h.svc.Queries.Registration.Handle(c.Request.Context(), *input)
	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{"error": err.Error()})
		return
	}
	c.SetCookie("refreshToken", tokens.RefreshToken, 3600, "*", "localhost", true, true)
	c.JSON(201, map[string]interface{}{
		"accessToken": tokens.AccessToken,
	})

}

func (h handler) Login(c *gin.Context) {
	input := &queries.LoginQuery{}
	if err := c.BindJSON(input); err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	if err := h.v.Struct(input); err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": err.Error()})
		return
	}
	tokens, err := h.svc.Queries.Login.Handle(c.Request.Context(), *input)
	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(500, gin.H{"error": err.Error()})
			return
		}
	}
	c.SetCookie("refreshToken", tokens.RefreshToken, 3600, "*", "localhost", true, true)
	c.JSON(201, map[string]interface{}{
		"accessToken": tokens.AccessToken,
	})
}
