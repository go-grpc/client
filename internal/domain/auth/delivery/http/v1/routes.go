package v1

import (
	"client/internal/domain/auth/service"
	"client/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

func NewAuthHttpEndPoints(g *gin.RouterGroup, log *logger.Logger, service *service.ProductService, v *validator.Validate) {
	r := g.Group("auth")
	handler := NewHandler(log, service, v)
	{
		r.POST("/login", handler.Login)
		r.POST("/registration", handler.Registration)
	}
}
