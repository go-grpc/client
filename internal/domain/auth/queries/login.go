package queries

import (
	"client/internal/adapter/api/auth/proto"
	"client/internal/domain/auth/dto"
	"client/pkg/logger"
	"context"
	"google.golang.org/grpc"
)

type loginGRPCQuery interface {
	Login(ctx context.Context, in *proto.AuthRequest, opts ...grpc.CallOption) (*proto.AuthResponse, error)
}

type LoginHandler interface {
	Handle(ctx context.Context, dto LoginQuery) (*dto.LoginResponse, error)
}

type loginHandler struct {
	log *logger.Logger
	svc loginGRPCQuery
}

func (l loginHandler) Handle(ctx context.Context, query LoginQuery) (*dto.LoginResponse, error) {
	user, err := l.svc.Login(ctx, &proto.AuthRequest{
		Phone:    query.Phone,
		Password: query.Password,
	})
	if err != nil {
		return nil, err
	}
	return dto.LoginResponseFromGRPC(user), err
}

func NewLoginHandler(svc loginGRPCQuery, log *logger.Logger) *loginHandler {
	return &loginHandler{log, svc}
}
