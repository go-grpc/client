package queries

import (
	"client/internal/adapter/api/auth/proto"
	"client/internal/domain/auth/dto"
	"client/pkg/logger"
	"context"
	"google.golang.org/grpc"
)

type RefreshGRPCQuery interface {
	Refresh(ctx context.Context, in *proto.RefreshRequest, opts ...grpc.CallOption) (*proto.RefreshResponse, error)
}

type RefreshHandler interface {
	Handle(ctx context.Context, query RefreshTokenQuery) (*dto.RefreshResponse, error)
}

type refreshHandler struct {
	log *logger.Logger
	svc RefreshGRPCQuery
}

func (l refreshHandler) Handle(ctx context.Context, query RefreshTokenQuery) (*dto.RefreshResponse, error) {
	res, err := l.svc.Refresh(ctx, &proto.RefreshRequest{
		RefreshToken: query.RefreshToken,
	})
	if err != nil {
		return nil, err
	}
	return dto.RefreshResponseFromGRPC(res), err
}

func NewRefreshHandler(svc RefreshGRPCQuery, log *logger.Logger) *refreshHandler {
	return &refreshHandler{log, svc}
}
