package queries

type LoginQuery struct {
	Phone    string `json:"phone" validate:"required"`
	Password string `json:"password" validate:"required"`
}

func NewLoginQuery(phone, password string) *LoginQuery {
	return &LoginQuery{phone, password}
}

type RefreshTokenQuery struct {
	RefreshToken string `json:"refresh_token" validate:"required"`
}

func NewRefreshTokenQuery(token string) *RefreshTokenQuery {
	return &RefreshTokenQuery{token}
}

type RegistrationQuery struct {
	Name     string `json:"name" validate:"required"`
	Phone    string `json:"phone" validate:"required"`
	Password string `json:"password" validate:"required"`
}
type AuthQuery struct {
	Login        LoginHandler
	Logout       LogoutHandler
	Refresh      RefreshHandler
	Registration RegistrationHandler
}

func NewAuthQuery(login LoginHandler, logout LogoutHandler, refresh RefreshHandler, registration RegistrationHandler) *AuthQuery {
	return &AuthQuery{login, logout, refresh, registration}
}
