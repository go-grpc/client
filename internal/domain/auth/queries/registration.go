package queries

import (
	"client/internal/adapter/api/auth/proto"
	"client/internal/domain/auth/dto"
	"client/pkg/logger"
	"context"
	"google.golang.org/grpc"
)

type RegisterGRPCQuery interface {
	Registration(ctx context.Context, in *proto.AuthRequest, opts ...grpc.CallOption) (*proto.AuthResponse, error)
}

type RegistrationHandler interface {
	Handle(ctx context.Context, query RegistrationQuery) (*dto.RegistrationResponse, error)
}

type registrationHandler struct {
	log *logger.Logger
	svc RegisterGRPCQuery
}

func NewRegisterHandler(svc RegisterGRPCQuery, log *logger.Logger) *registrationHandler {
	return &registrationHandler{log, svc}
}

func (l registrationHandler) Handle(ctx context.Context, query RegistrationQuery) (*dto.RegistrationResponse, error) {
	res, err := l.svc.Registration(ctx, &proto.AuthRequest{
		Phone:    query.Phone,
		Password: query.Password,
	})
	if err != nil {
		return nil, err
	}
	return dto.RegistrationResponseFromGRPC(res), nil
}
