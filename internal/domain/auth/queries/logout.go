package queries

import (
	"client/internal/adapter/api/auth/proto"
	"client/internal/domain/auth/dto"
	"client/pkg/logger"
	"context"
	"google.golang.org/grpc"
)

type logoutGrpcQuery interface {
	Logout(ctx context.Context, in *proto.LogoutRequest, opts ...grpc.CallOption) (*proto.LogoutResponse, error)
}

type LogoutHandler interface {
	Handle(ctx context.Context, query RefreshTokenQuery) (*dto.LogoutResponse, error)
}

type logoutHandler struct {
	log *logger.Logger
	svc logoutGrpcQuery
}

func (l logoutHandler) Handle(ctx context.Context, query RefreshTokenQuery) (*dto.LogoutResponse, error) {
	_, err := l.svc.Logout(ctx, &proto.LogoutRequest{
		RefreshToken: query.RefreshToken,
	})
	if err != nil {
		return nil, err
	}
	return nil, err
}

func NewLogoutHandler(svc logoutGrpcQuery, log *logger.Logger) *logoutHandler {
	return &logoutHandler{log, svc}
}
