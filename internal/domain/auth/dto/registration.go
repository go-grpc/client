package dto

import "client/internal/adapter/api/auth/proto"

type RegistrationResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

func RegistrationResponseFromGRPC(res *proto.AuthResponse) *RegistrationResponse {
	return &RegistrationResponse{res.AccessToken, res.RefreshToken}
}
