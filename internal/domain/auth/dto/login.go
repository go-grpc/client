package dto

import "client/internal/adapter/api/auth/proto"

type LoginResponse struct {
	RefreshToken string `json:"refresh_token"`
	AccessToken  string `json:"access_token"`
}

func LoginResponseFromGRPC(dto *proto.AuthResponse) *LoginResponse {
	return &LoginResponse{
		RefreshToken: dto.RefreshToken,
		AccessToken:  dto.AccessToken,
	}
}
