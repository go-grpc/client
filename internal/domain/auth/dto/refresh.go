package dto

import "client/internal/adapter/api/auth/proto"

type RefreshResponse struct {
	RefreshToken string `json:"refresh_token"`
	AccessToken  string `json:"access_token"`
}

func RefreshResponseFromGRPC(dto *proto.RefreshResponse) *RefreshResponse {
	return &RefreshResponse{
		RefreshToken: dto.RefreshToken,
		AccessToken:  dto.AccessToken,
	}
}
