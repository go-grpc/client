package service

import (
	"client/internal/adapter/api/auth/proto"
	"client/internal/domain/auth/queries"
	"client/pkg/logger"
)

type ProductService struct {
	Queries *queries.AuthQuery
}

func NewAuthService(log *logger.Logger, authClient proto.AuthServiceClient) *ProductService {
	login := queries.NewLoginHandler(authClient, log)
	logout := queries.NewLogoutHandler(authClient, log)
	refresh := queries.NewRefreshHandler(authClient, log)
	registration := queries.NewRegisterHandler(authClient, log)

	authQueries := queries.NewAuthQuery(login, logout, refresh, registration)
	return &ProductService{authQueries}
}
