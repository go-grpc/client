package dto

type UpdateOrderStatusDto struct {
	OrderId uint32 `json:"order_id"`
	Status  string `json:"status"`
}
