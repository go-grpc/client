package dto

type CreateOrderDto struct {
	UserId uint32 `json:"user_id" validate:"required"`
}
