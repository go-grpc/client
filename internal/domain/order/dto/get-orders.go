package dto

import "client/internal/adapter/api/order/proto"

type Order struct {
	Id         uint32 `json:"id"`
	UserId     uint32 `json:"user_id"`
	TotalPrice uint32 `json:"total_price"`
	Status     string `json:"status"`
}

type GetOrdersResponse struct {
	Orders []Order `json:"orders"`
}

func OrdersJSONFromGRPC(arg *proto.GetOrdersResponse) *GetOrdersResponse {
	orders := make([]Order, 0, len(arg.Orders))
	for _, order := range arg.Orders {
		orders = append(orders, Order{
			Id:         order.Id,
			TotalPrice: uint32(order.TotalPrice),
			Status:     order.Status,
		})
	}
	return &GetOrdersResponse{orders}
}
