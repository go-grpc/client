package dto

import "client/internal/adapter/api/order/proto"

type OrderOneResponse struct {
	Order
	CartItems []cartItem `json:"cart_items"`
}

func OrderOneResponseFromGrpc(dto *proto.GetOrderByIdResponse) *OrderOneResponse {
	return &OrderOneResponse{
		Order: Order{
			Id:         dto.Id,
			TotalPrice: uint32(dto.TotalPrice),
			Status:     dto.Status,
		},
		CartItems: cartItemsGrpcFromJson(dto.CartItems),
	}
}

type cartItem struct {
	Id        uint32 `json:"id"`
	ProductId string `json:"product_id"`
	Qty       uint32 `json:"qty"`
}

func cartItemsGrpcFromJson(arg []*proto.CartItem) []cartItem {
	items := make([]cartItem, 0, len(arg))
	for _, item := range arg {
		items = append(items, cartItem{
			Id:        item.Id,
			ProductId: item.ProductId,
			Qty:       item.Qty,
		})
	}
	return items
}
