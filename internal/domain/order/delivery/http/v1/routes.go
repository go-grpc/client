package v1

import (
	"client/internal/domain/order/service"
	"client/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

func NewOrderHttpEndPoints(g *gin.RouterGroup, log *logger.Logger, service *service.OrderService, v *validator.Validate) {
	r := g.Group("order")
	handler := NewHandler(log, service, v)
	{
		r.POST("/create", handler.CreateOrder)
		r.PUT("/update", handler.UpdateOrder)
		r.GET("/", handler.GetOrders)
		r.GET("/:id", handler.GetOrder)
	}
}
