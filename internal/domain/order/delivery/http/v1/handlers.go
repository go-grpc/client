package v1

import (
	"client/internal/domain/middlewares"
	"client/internal/domain/order"
	"client/internal/domain/order/comand"
	"client/internal/domain/order/dto"
	"client/internal/domain/order/queries"
	"client/internal/domain/order/service"
	"client/pkg/logger"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
	"strconv"
)

type handler struct {
	log *logger.Logger
	svc *service.OrderService
	v   *validator.Validate
}

func (h handler) CreateOrder(c *gin.Context) {
	param, _ := c.Get(middlewares.UserId)
	id, _ := strconv.ParseInt(fmt.Sprintf("%v", param), 10, 64)
	createDto := &dto.CreateOrderDto{UserId: uint32(id)}
	if err := h.v.Struct(createDto); err != nil {
		h.log.Warn("validate", err)
		c.AbortWithStatusJSON(422, gin.H{"message": "error validate", "error": err.Error()})
		return
	}
	if err := h.svc.Command.CreateOrder.Handle(c, comand.NewCreateOrderCommand(createDto)); err != nil {
		h.log.Warn("createOrder", err)
		c.AbortWithStatusJSON(500, gin.H{"message": "error create product", "error": err.Error()})
		return
	}

	c.JSON(200, map[string]interface{}{
		"user_id": param,
	})
}

func (h handler) UpdateOrder(c *gin.Context) {
	updateDto := &dto.UpdateOrderStatusDto{}
	if err := c.BindJSON(&updateDto); err != nil {
		h.log.Warn("Bind", err)
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	if err := h.v.Struct(updateDto); err != nil {
		h.log.Warn("validate", err)
		c.AbortWithStatusJSON(422, gin.H{"message": "error validate", "error": err.Error()})
		return
	}
	if err := h.svc.Command.UpdateOrder.Handle(c.Request.Context(), comand.NewUpdateOrderCommand(updateDto)); err != nil {
		h.log.Warn("update order", err)
		c.AbortWithStatusJSON(500, gin.H{"message": "error update product", "error": err.Error()})
		return
	}
	c.JSON(200, map[string]interface{}{
		"status": "ok",
	})
}

func (h handler) GetOrder(c *gin.Context) {
	param := c.Param("id")
	id, err := strconv.ParseInt(fmt.Sprintf("%v", param), 10, 64)
	if err != nil {
		h.log.Warn("intFromSting", err)
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	query := queries.NewGetOrderByIdQuery(int(id))
	response, err := h.svc.Query.GetOrder.Handle(c.Request.Context(), query)
	if err != nil {
		h.log.Warn("get order by id", err)
	}
	c.JSON(http.StatusOK, response)
}

func (h handler) GetOrders(c *gin.Context) {
	param, _ := c.Get(middlewares.UserId)
	id, err := strconv.ParseInt(fmt.Sprintf("%v", param), 10, 64)
	if err != nil {
		h.log.Warn("intFromSting", err)
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	query := queries.NewGetOrderQuery(int(id))
	response, err := h.svc.Query.GetOrders.Handle(c.Request.Context(), query)
	if err != nil {
		h.log.Warn("get orders", err)
	}
	c.JSON(http.StatusOK, response)
}

func NewHandler(log *logger.Logger, svc *service.OrderService, v *validator.Validate) order.HttpDelivery {
	return &handler{log, svc, v}
}
