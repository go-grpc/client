package service

import (
	"client/internal/adapter/api/order/proto"
	"client/internal/domain/config"
	"client/internal/domain/order/comand"
	"client/internal/domain/order/queries"
	"client/pkg/logger"
	"github.com/segmentio/kafka-go"
)

type OrderService struct {
	Query   *queries.OrderQueries
	Command *comand.OrderCommands
}

func NewOrderService(client proto.OrderServiceClient, writer *kafka.Writer, log *logger.Logger, cfg *config.Config) *OrderService {
	getOrderQuery := queries.NewGetOrderHandler(cfg, client, log)
	getOrdersQuery := queries.NewGetOrdersHandler(log, client, cfg)
	query := queries.NewOrderQueries(getOrderQuery, getOrdersQuery)

	createOrderCommand := comand.NewCreateOrderHandler(log, writer, cfg)
	updateOrderCommand := comand.NewUpdateOrderHandler(writer, cfg, log)
	commands := comand.NewOrderCommands(createOrderCommand, updateOrderCommand)

	return &OrderService{query, commands}
}
