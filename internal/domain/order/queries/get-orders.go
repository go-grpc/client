package queries

import (
	"client/internal/adapter/api/order/proto"
	"client/internal/domain/config"
	"client/internal/domain/order/dto"
	"client/pkg/logger"
	"context"
	"google.golang.org/grpc"
)

type getOrdersGRPCQuery interface {
	GetOrders(ctx context.Context, in *proto.GetOrdersRequest, opts ...grpc.CallOption) (*proto.GetOrdersResponse, error)
}
type GetOrdersHandler interface {
	Handle(ctx context.Context, arg *GetOrdersQuery) (*dto.GetOrdersResponse, error)
}

type getOrdersHandler struct {
	log *logger.Logger
	svc getOrdersGRPCQuery
	cfg *config.Config
}

func (h getOrdersHandler) Handle(ctx context.Context, arg *GetOrdersQuery) (*dto.GetOrdersResponse, error) {
	orders, err := h.svc.GetOrders(ctx, &proto.GetOrdersRequest{UserId: uint32(arg.UserId)})
	if err != nil {
		return nil, err
	}
	return dto.OrdersJSONFromGRPC(orders), err
}
func NewGetOrdersHandler(log *logger.Logger, svc getOrdersGRPCQuery, cfg *config.Config) *getOrdersHandler {
	return &getOrdersHandler{log, svc, cfg}
}
