package queries

type OrderQueries struct {
	GetOrder  GetOrderHandler
	GetOrders GetOrdersHandler
}
type GetOrderByIdQuery struct {
	OrderId int `json:"order_id" validate:"required"`
}

func NewGetOrderByIdQuery(id int) *GetOrderByIdQuery {
	return &GetOrderByIdQuery{id}
}

type GetOrdersQuery struct {
	UserId int `json:"user_id" validate:"required"`
}

func NewGetOrderQuery(userId int) *GetOrdersQuery {
	return &GetOrdersQuery{userId}
}
func NewOrderQueries(getOrder GetOrderHandler, getOrders GetOrdersHandler) *OrderQueries {
	return &OrderQueries{getOrder, getOrders}
}
