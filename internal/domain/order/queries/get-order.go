package queries

import (
	"client/internal/adapter/api/order/proto"
	"client/internal/domain/config"
	"client/internal/domain/order/dto"
	"client/pkg/logger"
	"context"
	"google.golang.org/grpc"
)

type getOrderGRPCQuery interface {
	GetOrderOne(ctx context.Context, in *proto.GetOrderByIdRequest, opts ...grpc.CallOption) (*proto.GetOrderByIdResponse, error)
}

type GetOrderHandler interface {
	Handle(ctx context.Context, dto *GetOrderByIdQuery) (*dto.OrderOneResponse, error)
}
type getOrderHandler struct {
	cfg *config.Config
	svc getOrderGRPCQuery
	log *logger.Logger
}

func (h getOrderHandler) Handle(ctx context.Context, arg *GetOrderByIdQuery) (*dto.OrderOneResponse, error) {
	order, err := h.svc.GetOrderOne(ctx, &proto.GetOrderByIdRequest{Id: uint32(arg.OrderId)})
	if err != nil {
		return nil, err
	}
	return dto.OrderOneResponseFromGrpc(order), nil
}

func NewGetOrderHandler(cfg *config.Config, svc getOrderGRPCQuery, log *logger.Logger) *getOrderHandler {
	return &getOrderHandler{cfg, svc, log}
}
