package comand

import "client/internal/domain/order/dto"

type CreateOrderCommand struct {
	CreateDto *dto.CreateOrderDto
}

func NewCreateOrderCommand(dto *dto.CreateOrderDto) *CreateOrderCommand {
	return &CreateOrderCommand{CreateDto: dto}
}

type UpdateOrderCommand struct {
	UpdateDto *dto.UpdateOrderStatusDto
}

func NewUpdateOrderCommand(dto *dto.UpdateOrderStatusDto) *UpdateOrderCommand {
	return &UpdateOrderCommand{dto}
}

type OrderCommands struct {
	CreateOrder CreateOrderCMDHandler
	UpdateOrder UpdateOrderCMDHandler
}

func NewOrderCommands(createOrder CreateOrderCMDHandler, updateOrder UpdateOrderCMDHandler) *OrderCommands {
	return &OrderCommands{createOrder, updateOrder}
}
