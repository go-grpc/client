package comand

import (
	"client/internal/domain/config"
	"client/pkg/logger"
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"time"
)

type CreateOrderCMDHandler interface {
	Handle(ctx context.Context, dto *CreateOrderCommand) error
}

type createOrderHandler struct {
	log      *logger.Logger
	producer *kafka.Writer
	config   *config.Config
}

func (h *createOrderHandler) Handle(ctx context.Context, dto *CreateOrderCommand) error {
	dtoBytes, err := json.Marshal(dto)
	if err != nil {
		return err
	}
	return h.producer.WriteMessages(ctx, kafka.Message{
		Topic: h.config.KafkaTopics.OrderTopics.OrderCreate.Topic,
		Value: dtoBytes,
		Time:  time.Now(),
	})
}

func NewCreateOrderHandler(log *logger.Logger, producer *kafka.Writer, config *config.Config) *createOrderHandler {
	return &createOrderHandler{log, producer, config}
}
