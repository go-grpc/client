package comand

import (
	"client/internal/domain/config"
	"client/pkg/logger"
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"time"
)

type UpdateOrderCMDHandler interface {
	Handle(ctx context.Context, dto *UpdateOrderCommand) error
}

type updateOrderHandler struct {
	producer *kafka.Writer
	cfg      *config.Config
	log      *logger.Logger
}

func (h updateOrderHandler) Handle(ctx context.Context, dto *UpdateOrderCommand) error {
	dtoBytes, err := json.Marshal(dto)
	if err != nil {
		h.log.Error("error in update order marshal")
	}

	return h.producer.WriteMessages(ctx, kafka.Message{
		Topic:   h.cfg.KafkaTopics.OrderUpdate.Topic,
		Value:   dtoBytes,
		Headers: nil,
		Time:    time.Now(),
	})
}

func NewUpdateOrderHandler(producer *kafka.Writer, cfg *config.Config, log *logger.Logger) *updateOrderHandler {
	return &updateOrderHandler{producer, cfg, log}
}
