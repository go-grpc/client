package order

import "github.com/gin-gonic/gin"

type HttpDelivery interface {
	CreateOrder(ctx *gin.Context)
	UpdateOrder(ctx *gin.Context)
	GetOrder(ctx *gin.Context)
	GetOrders(ctx *gin.Context)
}
