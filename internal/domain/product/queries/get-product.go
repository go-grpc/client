package queries

import (
	"client/internal/adapter/api/product/proto"
	"client/internal/domain/config"
	"client/internal/domain/product/dto"
	"client/pkg/logger"
	"context"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

type getProductHandlerGRPC interface {
	GetOrderOneById(ctx context.Context, in *proto.GetProductByIdRequest, opts ...grpc.CallOption) (*proto.GetProductOneResponse, error)
}

type GetProductHandler interface {
	Handle(ctx context.Context, dto *GetProductQuery) (*dto.Product, error)
}

type getProductHandler struct {
	log *logger.Logger
	svc getProductHandlerGRPC
	cfg *config.Config
}

func (h getProductHandler) Handle(ctx context.Context, data *GetProductQuery) (*dto.Product, error) {
	product, err := h.svc.GetOrderOneById(ctx, &proto.GetProductByIdRequest{Id: data.Id})
	if err != nil {
		return nil, err
	}
	if product.Response.Status > 200 {
		return nil, errors.New(product.Response.Error)
	}
	return dto.GetProductResponse(product.Product), nil
}

func NewGetProductHandler(log *logger.Logger, svc getProductHandlerGRPC, cfg *config.Config) *getProductHandler {
	return &getProductHandler{log, svc, cfg}
}
