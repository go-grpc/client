package queries

import (
	"client/internal/adapter/api/product/proto"
	"client/internal/domain/config"
	"client/internal/domain/product/dto"
	"client/pkg/logger"
	"context"
	"google.golang.org/grpc"
)

type getProductsHandlerGRPC interface {
	GetProducts(ctx context.Context, in *proto.GetProductsRequest, opts ...grpc.CallOption) (*proto.GetProductResponse, error)
}

type GetProductsHandler interface {
	Handle(ctx context.Context, arg *GetProductsQuery) ([]dto.Product, error)
}

type getProductsHandler struct {
	log *logger.Logger
	svc getProductsHandlerGRPC
	cfg *config.Config
}

func (h getProductsHandler) Handle(ctx context.Context, arg *GetProductsQuery) ([]dto.Product, error) {
	products, err := h.svc.GetProducts(ctx, &proto.GetProductsRequest{Categories: arg.Categories})
	if err != nil {
		h.log.Warn("error in get products handler")
		return nil, err
	}
	return dto.GetProductsResponseFromGRPC(products), nil
}

func NewGetProductsHandler(log *logger.Logger, svc getProductsHandlerGRPC, cfg *config.Config) *getProductsHandler {
	return &getProductsHandler{log, svc, cfg}
}
