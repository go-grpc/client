package queries

type GetProductsQuery struct {
	Categories []string `json:"categories"`
}

func NewGetProductsQuery(categories []string) *GetProductsQuery {
	return &GetProductsQuery{categories}
}

type GetProductQuery struct {
	Id string `json:"id" validate:"required"`
}

func NewGetProductQuery(id string) *GetProductQuery {
	return &GetProductQuery{id}
}

type ProductQueries struct {
	GetProduct  GetProductHandler
	GetProducts GetProductsHandler
}

func NewProductQueries(getProduct GetProductHandler, getProducts GetProductsHandler) *ProductQueries {
	return &ProductQueries{getProduct, getProducts}
}
