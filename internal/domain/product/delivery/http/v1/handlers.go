package v1

import (
	"client/internal/domain/product/comands"
	"client/internal/domain/product/dto"
	"client/internal/domain/product/queries"
	"client/internal/domain/product/service"
	"client/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"strings"
)

type handler struct {
	svc *service.ProductService
	log *logger.Logger
	v   *validator.Validate
}

func (h handler) CreateProduct(c *gin.Context) {
	createDto := &dto.CreateProductDto{}
	if err := c.BindJSON(createDto); err != nil {
		h.log.Warn("error create product handler in binding")
		c.AbortWithStatusJSON(500, gin.H{"error": "error in binding"})
		return
	}
	if err := h.v.Struct(createDto); err != nil {
		h.log.Warn("error product handler in validating")
		c.AbortWithStatusJSON(422, gin.H{"error": "error in validating", "validateError": err})
		return
	}
	if err := h.svc.Commands.Create.Handle(c.Request.Context(), comands.NewCreatProductCommand(createDto)); err != nil {
		h.log.Warn("error in create product command")
		c.AbortWithStatusJSON(500, gin.H{"error": "error in create product command"})
		return
	}
	c.JSON(201, map[string]interface{}{
		"status": "ok",
	})

}

func (h handler) DeleteProduct(c *gin.Context) {
	param := c.Param("id")
	deleteDto := &dto.DeleteProductDto{Id: param}
	if err := h.v.Struct(deleteDto); err != nil {
		h.log.Warn("error delete product handler in validating")
		c.AbortWithStatusJSON(422, gin.H{"error": "error in validating", "validateError": err})
		return
	}
	if err := h.svc.Commands.Delete.Handle(c.Request.Context(), comands.NewDeleteProductCommand(deleteDto)); err != nil {
		h.log.Warn("error in delete product command")
		c.AbortWithStatusJSON(500, gin.H{"error": "error in delete product command"})
		return
	}
	c.JSON(200, map[string]interface{}{
		"status": "ok",
	})
}

func (h handler) UpdateProduct(c *gin.Context) {
	param := c.Param("id")
	updateDto := &dto.UpdateProductDto{DeleteProductDto: dto.DeleteProductDto{Id: param}}
	if err := c.BindJSON(&updateDto.CreateProductDto); err != nil {
		h.log.Warn("error update product handler in binding")
		c.AbortWithStatusJSON(500, gin.H{"error": "error in binding"})
		return
	}
	if err := h.v.Struct(updateDto); err != nil {
		h.log.Warn("error update product handler in validating")
		c.AbortWithStatusJSON(422, gin.H{"error": "error in validating", "validateError": err})
		return
	}
	if err := h.svc.Commands.Update.Handle(c.Request.Context(), comands.NewUpdateProductCommand(updateDto)); err != nil {
		h.log.Warn("error in update product command")
		c.AbortWithStatusJSON(500, gin.H{"error": "error in update product command"})
		return
	}
	c.JSON(200, map[string]interface{}{
		"status": "ok",
	})
}

func (h handler) GetProduct(c *gin.Context) {
	param := c.Param("id")
	getOneDto := queries.NewGetProductQuery(param)
	if err := h.v.Struct(getOneDto); err != nil {
		h.log.Warn("error get product handler in param")
		c.AbortWithStatusJSON(422, gin.H{"error": "param is empty", "validateError": err})
		return
	}
	product, err := h.svc.Queries.GetProduct.Handle(c.Request.Context(), getOneDto)
	if err != nil {
		h.log.Warn("error in get product query")
		c.AbortWithStatusJSON(500, gin.H{"error": "error in get product query"})
		return
	}
	c.JSON(200, product)
}

func (h handler) GetProducts(c *gin.Context) {
	query := c.Query("categories")
	categories := strings.Split(query, ",")
	getDto := queries.NewGetProductsQuery(categories)
	data, err := h.svc.Queries.GetProducts.Handle(c.Request.Context(), getDto)
	if err != nil {
		h.log.Warn("error in get products query")
		c.AbortWithStatusJSON(500, gin.H{"error": "error in get products query"})
		return
	}
	c.JSON(200, data)
}

func NewHandler(svc *service.ProductService, log *logger.Logger, v *validator.Validate) *handler {
	return &handler{svc, log, v}
}
