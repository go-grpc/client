package v1

import (
	"client/internal/domain/product/service"
	"client/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

func NewProductHttpEndpoints(g *gin.RouterGroup, svc *service.ProductService, log *logger.Logger, v *validator.Validate) {
	h := NewHandler(svc, log, v)
	r := g.Group("product")
	{
		r.POST("/", h.CreateProduct)
		r.PUT("/:id", h.UpdateProduct)
		r.DELETE("/:id", h.DeleteProduct)
		r.GET("/", h.GetProducts)
		r.GET("/:id", h.GetProduct)
	}
}
