package comands

import "client/internal/domain/product/dto"

type CreateProductCommand struct {
	CreateDto *dto.CreateProductDto
}

func NewCreatProductCommand(dto *dto.CreateProductDto) *CreateProductCommand {
	return &CreateProductCommand{dto}
}

type UpdateProductCommand struct {
	UpdateDto *dto.UpdateProductDto
}

func NewUpdateProductCommand(dto *dto.UpdateProductDto) *UpdateProductCommand {
	return &UpdateProductCommand{dto}
}

type DeleteProductCommand struct {
	DeleteDto *dto.DeleteProductDto
}

func NewDeleteProductCommand(dto *dto.DeleteProductDto) *DeleteProductCommand {
	return &DeleteProductCommand{dto}
}

type ProductCommands struct {
	Create CreateProductHandler
	Update UpdateProductHandler
	Delete DeleteProductHandler
}

func NewProductCommands(create CreateProductHandler, update UpdateProductHandler, delete DeleteProductHandler) *ProductCommands {
	return &ProductCommands{create, update, delete}
}
