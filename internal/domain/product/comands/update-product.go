package comands

import (
	"client/internal/domain/config"
	"client/pkg/logger"
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"time"
)

type UpdateProductHandler interface {
	Handle(ctx context.Context, dto *UpdateProductCommand) error
}

type updateProductHandler struct {
	log      *logger.Logger
	producer *kafka.Writer
	cfg      *config.Config
}

func (h updateProductHandler) Handle(ctx context.Context, dto *UpdateProductCommand) error {
	dtoBytes, err := json.Marshal(dto)
	if err != nil {
		h.log.Warn("error marshal in update product handle")
		return err
	}
	return h.producer.WriteMessages(ctx, kafka.Message{
		Topic: h.cfg.KafkaTopics.ProductUpdate.Topic,
		Value: dtoBytes,
		Time:  time.Now(),
	})
}

func NewUpdateProductHandler(log *logger.Logger, producer *kafka.Writer, cfg *config.Config) *updateProductHandler {
	return &updateProductHandler{log, producer, cfg}
}
