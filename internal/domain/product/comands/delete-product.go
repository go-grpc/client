package comands

import (
	"client/internal/domain/config"
	"client/pkg/logger"
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"time"
)

type DeleteProductHandler interface {
	Handle(ctx context.Context, dto *DeleteProductCommand) error
}

type deleteProductHandler struct {
	log      *logger.Logger
	producer *kafka.Writer
	cfg      *config.Config
}

func (h deleteProductHandler) Handle(ctx context.Context, dto *DeleteProductCommand) error {
	dtoBytes, err := json.Marshal(dto)
	if err != nil {
		h.log.Warn("error marshal in delete product handler")
		return err
	}
	return h.producer.WriteMessages(ctx, kafka.Message{
		Topic: h.cfg.KafkaTopics.ProductDelete.Topic,
		Value: dtoBytes,
		Time:  time.Now(),
	})
}

func NewDeleteProductHandler(log *logger.Logger, producer *kafka.Writer, cfg *config.Config) *deleteProductHandler {
	return &deleteProductHandler{log, producer, cfg}
}
