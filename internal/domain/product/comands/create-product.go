package comands

import (
	"client/internal/domain/config"
	"client/pkg/logger"
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"time"
)

type CreateProductHandler interface {
	Handle(ctx context.Context, dto *CreateProductCommand) error
}

type createProductHandler struct {
	log      *logger.Logger
	producer *kafka.Writer
	cfg      *config.Config
}

func NewCreateProductHandle(log *logger.Logger, producer *kafka.Writer, cfg *config.Config) *createProductHandler {
	return &createProductHandler{log, producer, cfg}
}

func (h createProductHandler) Handle(ctx context.Context, dto *CreateProductCommand) error {
	dtoBytes, err := json.Marshal(dto)
	if err != nil {
		h.log.Warn("error marshal in create product")
		return err
	}
	return h.producer.WriteMessages(ctx, kafka.Message{
		Topic: h.cfg.KafkaTopics.ProductCreate.Topic,
		Value: dtoBytes,
		Time:  time.Now(),
	})
}
