package dto

type CreateProductDto struct {
	Price       int      `json:"price" validate:"required"`
	Name        string   `json:"name" validate:"required"`
	Description string   `json:"description" validate:"required"`
	Image       string   `json:"image" validate:"required"`
	Categories  []string `json:"categories" validate:"required"`
}
