package dto

import "client/internal/adapter/api/product/proto"

func GetProductsResponseFromGRPC(arg *proto.GetProductResponse) []Product {
	products := make([]Product, 0, len(arg.Products))
	for _, product := range products {
		products = append(products, Product{
			Id:          product.Id,
			Price:       product.Price,
			Name:        product.Name,
			Description: product.Description,
			Image:       product.Image,
			Categories:  product.Categories,
		})
	}
	return products

}
