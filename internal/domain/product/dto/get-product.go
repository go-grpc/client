package dto

import "client/internal/adapter/api/product/proto"

type Product struct {
	Id          string   `json:"id"`
	Price       int      `json:"price"`
	Name        string   `json:"name" `
	Description string   `json:"description"`
	Image       string   `json:"image" `
	Categories  []string `json:"categories"`
}

func GetProductResponse(data *proto.ProductItem) *Product {
	return &Product{
		Id:          data.Id,
		Price:       int(data.Price),
		Name:        data.Name,
		Description: data.Description,
		Image:       data.Image,
		Categories:  data.Categories,
	}
}
