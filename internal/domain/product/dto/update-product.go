package dto

type UpdateProductDto struct {
	DeleteProductDto
	CreateProductDto
}
