package dto

type DeleteProductDto struct {
	Id string `json:"id" validate:"required"`
}
