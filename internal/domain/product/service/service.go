package service

import (
	"client/internal/adapter/api/product/proto"
	"client/internal/domain/config"
	"client/internal/domain/product/comands"
	"client/internal/domain/product/queries"
	"client/pkg/logger"
	"github.com/segmentio/kafka-go"
)

type ProductService struct {
	Queries  *queries.ProductQueries
	Commands *comands.ProductCommands
}

func NewProductService(log *logger.Logger, writer *kafka.Writer, cfg *config.Config, client proto.ProductServiceClient) *ProductService {
	productQuery := queries.NewGetProductHandler(log, client, cfg)
	productsQuery := queries.NewGetProductsHandler(log, client, cfg)
	query := queries.NewProductQueries(productQuery, productsQuery)

	createProductCommand := comands.NewCreateProductHandle(log, writer, cfg)
	updateProductCommand := comands.NewUpdateProductHandler(log, writer, cfg)
	deleteProductCommand := comands.NewDeleteProductHandler(log, writer, cfg)
	command := comands.NewProductCommands(createProductCommand, updateProductCommand, deleteProductCommand)

	return &ProductService{query, command}
}
