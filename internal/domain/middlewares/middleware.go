package middlewares

import (
	"client/internal/adapter/api/auth/proto"
	"client/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"strings"
	"time"
)

const UserId = "userId"

type parseFromGrpc interface {
	ParseToken(ctx context.Context, in *proto.ParsTokenRequest, opts ...grpc.CallOption) (*proto.ParsTokenResponse, error)
}

func RequestLoggerMiddleware(log *logger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		c.Next()
		req := c.Request
		res := c.Request.Response
		status := res.Status
		size := c.Writer.Size()
		s := time.Since(start)

		log.HttpMiddleWare(req.Method, req.RequestURI, status, size, s)
	}
}
func Check(parse parseFromGrpc) gin.HandlerFunc {
	return func(c *gin.Context) {
		bearerToken := c.Request.Header.Get("Authorization")
		if bearerToken == "" {
			c.Next()
			return
		}
		token := strings.Split(bearerToken, "")

		if len(token) != 2 {
			c.Next()
			return
		}
		res, err := parse.ParseToken(c.Request.Context(), &proto.ParsTokenRequest{Token: token[1]})
		if err != nil {
			c.Next()
			return
		}
		c.Set(UserId, res.UserId)
		c.Next()
	}
}
