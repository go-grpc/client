package config

import (
	"flag"
	"fmt"
	"github.com/pkg/errors"
	"github.com/segmentio/kafka-go"
	"github.com/spf13/viper"
	"os"
)

var configPath string

func init() {
	flag.StringVar(&configPath, "config", "", "Api gateway")
}

type Config struct {
	ServiceName string      `mapstructure:"serviceName"`
	Http        Http        `mapstructure:"http"`
	Grpc        Grpc        `mapstructure:"grpc"`
	KafkaTopics KafkaTopics `mapstructure:"kafkaTopics"`
}

type Http struct {
	Port                string   `mapstructure:"port"`
	Development         bool     `mapstructure:"development"`
	BasePath            string   `mapstructure:"basePath"`
	ProductsPath        string   `mapstructure:"productsPath"`
	OrdersPath          string   `mapstructure:"ordersPath"`
	AuthPath            string   `mapstructure:"authPath"`
	DebugHeaders        bool     `mapstructure:"debugHeaders"`
	HttpClientDebug     bool     `mapstructure:"httpClientDebug"`
	DebugErrorsResponse bool     `mapstructure:"debugErrorsResponse"`
	IgnoreLogUrls       []string `mapstructure:"ignoreLogUrls"`
}
type OrderTopics struct {
	OrderCreate kafka.TopicConfig `mapstructure:"orderCreate"`
	OrderUpdate kafka.TopicConfig `mapstructure:"orderUpdate"`
}
type ProductTopics struct {
	ProductCreate kafka.TopicConfig `mapstructure:"productCreate"`
	ProductUpdate kafka.TopicConfig `mapstructure:"productUpdate"`
	ProductDelete kafka.TopicConfig `mapstructure:"productDelete"`
}
type CartTopics struct {
	CreateCartItem     kafka.TopicConfig `mapstructure:"createCartItem"`
	DeleteCart         kafka.TopicConfig `mapstructure:"deleteCart"`
	AddCartItemToOrder kafka.TopicConfig `mapstructure:"addCartItemToOrder"`
	MinceQty           kafka.TopicConfig `mapstructure:"minceQty"`
	AddQty             kafka.TopicConfig `mapstructure:"addQty"`
}
type Grpc struct {
	MarketServicePort string `mapstructure:"marketServicePort"`
	AuthServicePort   string `mapstructure:"authServicePort"`
	UserServicePort   string `mapstructure:"userServicePort"`
	OrderServicePort  string `mapstructure:"orderServicePort"`
}

type KafkaTopics struct {
	CartTopics
	OrderTopics
	ProductTopics
}

func InitConfig() (*Config, error) {
	if configPath == "" {
		configPathFromEnv := os.Getenv("CONFIG_PATH")
		if configPathFromEnv != "" {
			configPath = configPathFromEnv
		} else {
			getwd, err := os.Getwd()
			if err != nil {
				return nil, errors.Wrap(err, "os.Getwd")
			}
			configPath = fmt.Sprintf("%s/api_gateway_service/config/config.yaml", getwd)
		}
	}
	cfg := &Config{}
	viper.SetConfigType("yaml")
	viper.SetConfigFile(configPath)

	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "viper.ReadInConfig")
	}

	if err := viper.Unmarshal(cfg); err != nil {
		return nil, errors.Wrap(err, "viper.Unmarshal")
	}

	httpPort := os.Getenv("PORT")
	if httpPort != "" {
		cfg.Http.Port = httpPort
	}
	return cfg, nil
}
