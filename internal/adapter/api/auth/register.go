package auth

import (
	"client/internal/adapter/api/auth/proto"
	"github.com/gin-gonic/gin"
)

func HttpEndpoints(r *gin.RouterGroup, c proto.AuthServiceClient) {
	h := NewHandler(c)
	router := r.Group("/auth")

	{
		router.POST("/registration", h.Registration)
		router.POST("/login", h.Login)
		router.DELETE("/logout", h.Logout)
		router.GET("/refresh", h.Refresh)
		router.GET("/parse", h.Parse)

	}
}
