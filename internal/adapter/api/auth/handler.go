package auth

import (
	"client/internal/adapter/api/auth/proto"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct {
	c proto.AuthServiceClient
}

func NewHandler(c proto.AuthServiceClient) *Handler {
	return &Handler{c}
}

func (h Handler) Registration(c *gin.Context) {
	var input RegistrationDto
	if err := c.BindJSON(&input); err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	user, err := h.c.Registration(c.Request.Context(), &proto.AuthRequest{
		Phone:    input.Phone,
		Password: input.Password,
	})
	if err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": err})
		return
	}
	c.SetCookie("refreshToken", user.RefreshToken, 3600, "*", "localhost", true, true)
	c.JSON(200, map[string]interface{}{
		"response": "ok",
	})
}

func (h Handler) Login(c *gin.Context) {
	var input RegistrationDto
	if err := c.BindJSON(&input); err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	user, err := h.c.Login(c.Request.Context(), &proto.AuthRequest{
		Phone:    input.Phone,
		Password: input.Password,
	})
	if err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": err})
		return
	}
	c.SetCookie("refreshToken", user.RefreshToken, 3600, "*", "localhost", true, true)
	c.JSON(200, map[string]interface{}{
		"token": user.AccessToken,
	})
}

func (h Handler) Logout(c *gin.Context) {

	f, err := c.Request.Cookie("refreshToken")
	_, err = h.c.Logout(c.Request.Context(), &proto.LogoutRequest{
		RefreshToken: f.Value,
	})
	fmt.Println(f.HttpOnly)
	if err != nil {
		c.AbortWithStatusJSON(401, gin.H{"error": err})
		return
	}
	c.SetCookie("refreshToken", "", 0, "*", "localhost", true, true)
	c.JSON(200, map[string]interface{}{
		"message": "ok",
	})
}

func (h Handler) Refresh(c *gin.Context) {

	refreshToken, err := c.Request.Cookie("refreshToken")
	if err != nil {
		c.AbortWithStatusJSON(401, gin.H{"error": "вы не авторизованы"})
		return
	}
	user, err := h.c.Refresh(c.Request.Context(), &proto.RefreshRequest{
		RefreshToken: refreshToken.Value,
	})
	if err != nil {
		c.AbortWithStatusJSON(401, gin.H{"error": err})
		return
	}
	cookie := &http.Cookie{
		Name:     "refreshToken",
		Value:    user.GetRefreshToken(),
		Path:     "*",
		MaxAge:   3600,
		HttpOnly: true,
		Secure:   true,
		SameSite: http.SameSiteLaxMode,
	}
	http.SetCookie(c.Writer, cookie)
	c.JSON(200, map[string]interface{}{
		"token": user.AccessToken,
	})

}

func (h Handler) Parse(c *gin.Context) {

	var input ParseDto
	if err := c.BindJSON(&input); err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	user, err := h.c.ParseToken(c.Request.Context(), &proto.ParsTokenRequest{
		Token: input.Token,
	})
	if err != nil {
		c.AbortWithStatusJSON(401, gin.H{"error": err})
		return
	}
	c.JSON(200, map[string]interface{}{
		"token": user.GetUserId(),
	})

}
