package auth

type RegistrationDto struct {
	Phone    string `json:"phone"`
	Password string `json:"password"`
}
type ParseDto struct {
	Token string `json:"token"`
}
