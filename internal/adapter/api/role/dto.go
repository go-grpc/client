package role

type CreateRoleDto struct {
	Value string `json:"value"`
	Description string `json:"description"`
}
