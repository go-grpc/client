package role

import (
	"client/internal/adapter/proto"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v9"
	"time"
)

type Handler struct {
	c proto.RoleServiceClient
	redis  *redis.Client
}

func NewHandler(c proto.RoleServiceClient,redis *redis.Client) *Handler {
	return &Handler{c,redis}
}

func (h Handler) CreateRole(c *gin.Context) {
	var input CreateRoleDto
	if err := c.BindJSON(&input); err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	response, err := h.c.CreateRole(c.Request.Context(), &proto.CreateRoleRequest{
		Value:       input.Value,
		Description: input.Description,
	})
	if err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": err.Error()})
		return
	}
	c.JSON(201, response)

}

func (h Handler) GetRoles(c *gin.Context) {
	//var input CreateRoleDto
	//if err := c.BindJSON(&input);err !=nil{
	//	c.AbortWithStatusJSON(422,gin.H{"error":"не правильные данные"})
	//	return
	//}
	response, err := h.redis.Get(context.TODO(), "roles").Result()
	if err  == redis.Nil {
		fmt.Println("nil")
		response, err := h.c.GetRoles(c.Request.Context(), &proto.GetRolesRequest{})
		if err != nil {
			c.AbortWithStatusJSON(422, gin.H{"error": err.Error()})
			return
		}
		data ,_ :=json.Marshal(response)
		err = h.redis.Set(context.TODO(),"roles", data,time.Minute * 5).Err()
		if err != nil {
			panic(err)
		}
	}
	var res *proto.GetRolesResponse
	err = json.Unmarshal([]byte(response),&res)
	c.JSON(200, res)

}
