package role

import (
	"client/internal/adapter/proto"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v9"
)

func HttpEndpoints(r *gin.RouterGroup, c proto.RoleServiceClient, redis *redis.Client) {
	h := NewHandler(c,redis)
	router := r.Group("/role")
	{
		router.POST("/", h.CreateRole)
		router.GET("/", h.GetRoles)
	}
}
