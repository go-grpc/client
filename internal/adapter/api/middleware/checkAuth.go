package middleware

import "github.com/gin-gonic/gin"

func CheckAuth(c *gin.Context) {
	_, exist := c.Get(UserId)
	if !exist {
		c.AbortWithStatusJSON(401, gin.H{"message": "вы не авторизованы"})
		return
	}
	c.Next()
}
