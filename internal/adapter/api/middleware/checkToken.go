package middleware

import (
	"client/internal/adapter/api/auth/proto"
	"context"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"strings"
)

const UserId = "userId"

type IValidate interface {
	ParseToken(ctx context.Context, in *proto.ParsTokenRequest, opts ...grpc.CallOption) (*proto.ParsTokenResponse, error)
}

type Middleware struct {
	validate IValidate
}

func CheckMiddleWare(validate IValidate) gin.HandlerFunc {
	return func(c *gin.Context) {
		bearerToken := c.Request.Header.Get("Authorization")
		if bearerToken == "" {
			c.Next()
			return
		}
		token := strings.Split(bearerToken, "")

		if len(token) != 2 {
			c.Next()
			return
		}
		res, err := validate.ParseToken(c.Request.Context(), &proto.ParsTokenRequest{Token: token[1]})
		if err != nil {
			c.Next()
			return
		}
		c.Set(UserId, res.UserId)
		c.Next()
	}
}
