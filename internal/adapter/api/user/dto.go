package user

type CreateRoleDto struct {
	Value string `json:"value"`
	Description string `json:"description"`
}
