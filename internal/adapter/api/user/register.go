package user

import (
	"client/internal/adapter/proto"
	"github.com/gin-gonic/gin"
)

func HttpEndpoints(r *gin.RouterGroup, c proto.UserServiceClient) {
	h := NewHandler(c)
	router := r.Group("/user")
	{
		router.GET("/me/:id", h.GetUserById)
	}
}
