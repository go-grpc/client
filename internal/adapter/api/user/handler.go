package user

import (
	"client/internal/adapter/proto"
	"github.com/gin-gonic/gin"
	"strconv"
)

type Handler struct {
	c proto.UserServiceClient
}

func NewHandler(c proto.UserServiceClient) *Handler {
	return &Handler{c}
}

func (h Handler) GetUserById(c *gin.Context) {
	param := c.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": "параметр введен не правильно"})
		return
	}
	user, err := h.c.GetUserById(c.Request.Context(), &proto.GetUserByIdRequest{UserId: int64(id)})
	if err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": err.Error()})
		return
	}

	c.JSON(200, user)
}
