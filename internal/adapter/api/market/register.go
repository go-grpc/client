package market

import (
	"client/internal/adapter/api/market/proto"
	"github.com/gin-gonic/gin"
)

func HttpEndpoints(r *gin.RouterGroup, c proto.MarketServiceClient) {
	h := NewHandler(c)
	router := r.Group("/market")
	{
		router.POST("/", h.Create)
		router.GET("/:id",h.GetOne)
		router.PUT("/:id",h.Update)
		router.DELETE("/:id",h.Delete)
		router.PUT("/confirm/:id",h.Confirm)
	}
}

