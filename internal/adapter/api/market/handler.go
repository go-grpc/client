package market

import (
	"client/internal/adapter/api/market/proto"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	s proto.MarketServiceClient
}

func NewHandler(s proto.MarketServiceClient) *Handler {
	return &Handler{s}
}

func (h Handler) Create(c *gin.Context) {
	var input CreateMarketDto
	if err := c.BindJSON(&input); err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	res, err := h.s.CreateMarket(c.Request.Context(), &proto.CreateMarketRequest{
		Name:    input.Name,
		Image:   input.Image,
		OwnerId: input.OwnerId,
		Address: input.Address,
		BIN:     input.BIN,
	})
	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{"error": err.Error()})
		return
	}
	if res.Response.Status > 201 {
		c.AbortWithStatusJSON(int(res.Response.Status), gin.H{"error": res.Response.Error})
		return
	}
	c.JSON(int(res.Response.Status), map[string]interface{}{
		"id":     res.Id,
		"response": res.Response,
	})
}
func (h Handler) GetOne(c *gin.Context) {
    id := c.Param("id")
    if id == ""{
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	res,err := h.s.GetOneMarket(c.Request.Context(),&proto.GetOneMarketRequest{Id: id})
	if err !=nil{
		c.AbortWithStatusJSON(500, gin.H{"error": err.Error()})
		return
	}
	if res.Response.Status > 200 {
		c.AbortWithStatusJSON(int(res.Response.Status), gin.H{"response": res.Response})
		return
	}
	c.JSON(int(res.Response.Status), map[string]interface{}{
		"market":  res.Market,
		"response": res.Response,
	})
}
func (h Handler) Update(c *gin.Context) {
	id := c.Param("id")
	if id == ""{
		c.AbortWithStatusJSON(422, gin.H{"error": "параметр указан не правильно"})
		return
	}
	var input UpdateMarketDto
	if err := c.BindJSON(&input); err != nil {
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	res,err := h.s.UpdateMarket(c.Request.Context(),&proto.UpdateMarketRequest{
		Name:    input.Name,
		Address: input.Address,
		BIN:     input.BIN,
		Id:      id,
	})
	if err !=nil{
		c.AbortWithStatusJSON(500, gin.H{"error": err.Error()})
		return
	}
	if res.Status > 200{
		c.AbortWithStatusJSON(int(res.Status), gin.H{"error": res.Error})
		return
	}
	c.JSON(int(res.Status),res)
}

func (h Handler) Delete(c *gin.Context) {
	id := c.Param("id")
	if id == ""{
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	res,err := h.s.DeleteMarket(c.Request.Context(),&proto.DeleteMarketRequest{Id: id})
	if err !=nil{
		c.AbortWithStatusJSON(500, gin.H{"error": err.Error()})
		return
	}
	if res.Status > 200 {
		c.AbortWithStatusJSON(int(res.Status), gin.H{"error": res.Error})
		return
	}
	c.JSON(int(res.Status), res)
}

func (h Handler) Confirm(c *gin.Context) {
	id := c.Param("id")
	if id == ""{
		c.AbortWithStatusJSON(422, gin.H{"error": "не правильные данные"})
		return
	}
	res,err := h.s.ConfirmMarket(c.Request.Context(),&proto.ConfirmMarketRequest{Id: id})
	if err !=nil{
		c.AbortWithStatusJSON(500, gin.H{"error": err.Error()})
		return
	}
	if res.Status > 200 {
		c.AbortWithStatusJSON(int(res.Status), gin.H{"error": res.Error})
		return
	}
	c.JSON(int(res.Status), res)
}
