package market

type CreateMarketDto struct {
	Name string `json:"name"`
	Image string `json:"image"`
	OwnerId uint32 `json:"owner_id"`
	Address string `json:"address"`
	BIN string `json:"bin"`
}
type UpdateMarketDto struct {
	BIN string `json:"bin"`
	Address string `json:"address"`
	Name string `json:"name"`
}
