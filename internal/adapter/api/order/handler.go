package order

import (
	"client/internal/adapter/api/middleware"
	"client/internal/adapter/api/order/proto"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	kafkago "github.com/segmentio/kafka-go"
	"strconv"
)

type Handler struct {
	s proto.OrderServiceClient
	w *kafkago.Writer
}

func NewHandler(s proto.OrderServiceClient, r *kafkago.Writer) Handler {
	return Handler{s, r}
}

func (h *Handler) GetOrders(c *gin.Context) {
	param, _ := c.Get(middleware.UserId)
	id, err := strconv.ParseInt(fmt.Sprintf("%v", param), 10, 64)
	if err != nil {
		c.AbortWithStatusJSON(422, gin.H{"message": "не правильные данные"})
	}
	res, err := h.s.GetOrders(c.Request.Context(), &proto.GetOrdersRequest{UserId: uint32(id)})
	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{"message": err.Error()})
	}
	status := int(res.Response.Status)
	if res.Response.Status > 201 {
		c.AbortWithStatusJSON(status, gin.H{"message": res.Response.Error})
	}
	c.JSON(status, map[string]interface{}{
		"orders": res.Orders,
	})
}

func (h *Handler) GetOrderOneOrder(c *gin.Context) {
	param := c.Param("id")
	id, err := strconv.ParseUint(param, 32, 16)
	if err != nil {
		c.AbortWithStatusJSON(422, gin.H{"message": "не правильные данные"})
	}
	res, err := h.s.GetOrderOne(c.Request.Context(), &proto.GetOrderByIdRequest{Id: uint32(id)})
	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{"message": err.Error()})
	}
	status := int(res.Response.Status)
	if res.Response.Status > 201 {
		c.AbortWithStatusJSON(status, gin.H{"message": res.Response.Error})
	}
	c.JSON(status, map[string]interface{}{
		"id":          res.Id,
		"status":      res.Status,
		"total_price": res.TotalPrice,
		"cart_items":  res.CartItems,
	})
}

func (h *Handler) CreateOrder(c *gin.Context) {
	param, _ := c.Get(middleware.UserId)
	id, err := strconv.ParseInt(fmt.Sprintf("%v", param), 10, 64)
	dto := CreateOrderDto{UserId: uint32(id)}
	marshalDto, err := json.Marshal(dto)
	err = h.w.WriteMessages(c.Request.Context(), kafkago.Message{Topic: "CREATE_ORDER", Value: marshalDto})
	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{"message": err.Error()})
	}

	c.JSON(200, map[string]interface{}{
		"user_id": param,
	})
}

func (h *Handler) UpdateOrder(c *gin.Context) {
	param := c.Param("id")
	id, err := strconv.ParseInt(fmt.Sprintf("%v", param), 10, 64)
	dto := CreateOrderDto{UserId: uint32(id)}
	marshalDto, err := json.Marshal(dto)
	err = h.w.WriteMessages(c.Request.Context(), kafkago.Message{Topic: "CREATE_ORDER", Value: marshalDto})
	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{"message": err.Error()})
	}

	c.JSON(200, map[string]interface{}{
		"user_id": param,
	})
}
