package order

import (
	"client/internal/adapter/api/middleware"
	"client/internal/adapter/api/order/proto"
	"github.com/gin-gonic/gin"
	kafkago "github.com/segmentio/kafka-go"
)

func HttpEndpoints(g *gin.RouterGroup, c proto.OrderServiceClient, r *kafkago.Writer) {
	h := NewHandler(c, r)
	router := g.Group("orders")
	{
		router.POST("/create", middleware.CheckAuth, h.CreateOrder)
		router.PUT("/update")
		router.GET("one/:id", h.GetOrderOneOrder)
		router.GET("/", middleware.CheckAuth, h.GetOrders)
	}
}
