package order

type CreateOrderDto struct {
	UserId uint32 `json:"user_id"`
}
type UpdateOrderStatusDto struct {
	OrderId uint32 `json:"order_id"`
	Status  string `json:"status"`
}
