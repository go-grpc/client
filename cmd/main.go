package main

import (
	"client/internal/adapter/api/auth"
	proto2 "client/internal/adapter/api/auth/proto"
	"client/internal/adapter/api/market"
	proto3 "client/internal/adapter/api/market/proto"
	"client/internal/adapter/api/middleware"
	"client/internal/adapter/api/order"
	proto4 "client/internal/adapter/api/order/proto"
	proto5 "client/internal/adapter/api/product/proto"
	"client/internal/adapter/api/role"
	"client/internal/adapter/api/user"
	"client/internal/adapter/proto"
	"client/internal/domain/config"
	v1 "client/internal/domain/order/delivery/http/v1"
	"client/internal/domain/order/service"
	v2 "client/internal/domain/product/delivery/http/v1"
	service2 "client/internal/domain/product/service"
	"client/pkg/kafka"
	"client/pkg/logger"
	"client/pkg/server"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/go-redis/redis/v9"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	err := godotenv.Load(".env")
	userConn, err := grpc.Dial(":"+os.Getenv("USER_PORT"), grpc.WithInsecure())
	authConn, err := grpc.Dial(":"+os.Getenv("AUTH_PORT"), grpc.WithInsecure())
	marketConn, err := grpc.Dial(":"+os.Getenv("MARKER_PORT"), grpc.WithInsecure())
	orderConn, err := grpc.Dial(":"+os.Getenv("ORDER_PORT"), grpc.WithInsecure())
	productCoon, err := grpc.Dial(":"+os.Getenv("PRODUCT_PORT"), grpc.WithInsecure())
	rdb := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_HOST") + ":" + os.Getenv("REDIS_PORT"),
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	if err != nil {
		fmt.Println(err)
	}
	userClient := proto.NewUserServiceClient(userConn)
	roleClient := proto.NewRoleServiceClient(userConn)
	authClient := proto2.NewAuthServiceClient(authConn)
	marketClient := proto3.NewMarketServiceClient(marketConn)
	orderClient := proto4.NewOrderServiceClient(orderConn)
	productClient := proto5.NewProductServiceClient(productCoon)
	l := logger.New("debug")
	producer := kafka.New("localhost:9092")
	router := gin.Default()
	api := router.Group("/api", middleware.CheckMiddleWare(authClient))
	cfg, err := config.InitConfig()
	orderService := service.NewOrderService(orderClient, producer, l, cfg)
	productService := service2.NewProductService(l, producer, cfg, productClient)
	v := validator.New()
	v1.NewOrderHttpEndPoints(api, l, orderService, v)
	v2.NewProductHttpEndpoints(api, productService, l, v)
	user.HttpEndpoints(api, userClient)
	role.HttpEndpoints(api, roleClient, rdb)
	auth.HttpEndpoints(api, authClient)
	market.HttpEndpoints(api, marketClient)
	order.HttpEndpoints(api, orderClient, producer)
	srv := new(server.Server)

	go func() {
		if err := srv.Run(os.Getenv("PORT"), router); err != nil {
			log.Fatalf("что то поошло не так %s", err.Error())
		}
	}()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)

	<-quit
	log.Println("Shutting down")
	if err := srv.ShutDown(context.Background()); err != nil {
		log.Fatalf("error on shutting down: %s", err.Error())
	}

}
