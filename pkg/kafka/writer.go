package kafka

import kafkago "github.com/segmentio/kafka-go"

func New(addr string) *kafkago.Writer {
	return &kafkago.Writer{Addr: kafkago.TCP(addr)}
}
